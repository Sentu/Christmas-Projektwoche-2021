![](https://getlogo.net/wp-content/uploads/2020/05/ibz-schulen-ag-logo-vector.png)
# Projektwoche IBZ
## Inhaltsverzeichnis
### 1. [GitHub](#GitHub)
### 2. [GitLab](#GitLab)
### 3. [DevOps](#DevOps)
### 4. [Markdown](#Markdown)


**GitHub**

- GitHub wurde von PJ Hyett, Chris Wanstrath, Scott Chacon und Tom Preston-Werner entwickelt

- GitHub ist eine Code-Hosting-Plattform.

- Der Name kommt vom Versionsverwaltungssystem Git.

- Ähnliche Pattformen sind GitLab/Gitee/Bitbucket.

- Im Jahr 2018 wurde GitHub von Microsoft gekauft.

- GitHub ist eine Art Soziales Netzwerk für Informatiker.

- Es ist Cloud basiert und für jeden Gratis verwendbar.

- Informatiker können dies gut bei ihrer Arbeit nutzen um Inspiration zu sammeln oder Feedback für ihre Arbeit zu erhalten.

- Jeder kann Programmiercodes Hochladen und sie somit für andere Informatiker zugänglich machen.

- GitHub ist für Gruppenarbeiten sehr gut verwenbar, da es Cloudbasiert ist und jeder daran arbeiten kann.

- Sie können jederzeit die neueste Version des Projekts auf Github hochladen,
  damit alle Arbeitskollegen auf dem neuesten Stand sind.

**GitLab**

- Online seit 2012

- Für Software auf Git-Basis

- Ähnliche Pattformen sind GitHub/Gitee/Bitbucket.

- Wird vorallem in der Software-Entwicklung genutzt.

- GitLab ist eine Webbasierte Software.

- GitLab ist für jeden kostenlos verwendbar.

- Im vergleich zu GitHub auch Privat verwendbar.

- GitLab gibt einem die möglichkeit Codes online zu speichern.

**DevOps**

Devops setzt sich aus den Wörtern Development und Operations zusammmen.

Development = Entwicklung

Operation = Vorgänge

- Bei Projekten bei denen Systemtechniker und Applikationsentwickler zusammenarbeiten wird meist DevOps genutzt.

- Mit DevOps können Sie Projekte schnell und effizient miteinander teilen.

- DevOps ist kostenlos und ermöglicht Informatikern eine effiziente Zusammenarbeit, dabei spielt die Grösse des Projekts keine Rolle.

- DevOps beschäftigt sich mit der Entwicklung von Anwendungen, wie eine Anwendung entwickelt wird und was dabei verbessert werden kann.

- Es ist eine Sammlung unterschiedlicher technischer Methoden für die Zusammenarbeit zwischen Softwareentwicklung und IT-Betrieb.

- DevOps zielt auf eine effektivere und effizientere Zusammenarbeit zwischen Softwareentwicklung, Systemadministratoren und Qualitätssicherungsbereichen ab.

**Markdown**

- Markdown ist einfach anzuwenden.

- Markdown ist eine gute Alternative zu Word.

- Übersichtliche Listen und Tabellen.

- Markdown ähnelt HTML, ist aber simpler. 

- Sie können auch einen HTML-Code in Markdown schreiben.

- Markdown wird hauptsächlich für Entwicklerplattformen wie GitHub verwendet oder um Readme-Dateien zu erstellen.

- Das Ziel von Markdown ist es, die Präsentation vor der Konvertierung leicht lesbar zu machen.

- Es ist eine vereinfachte Auszeichnungssprache, die von John Gruber und Aaron Swartz entwickelt wurde.

