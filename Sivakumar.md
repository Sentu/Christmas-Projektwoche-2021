# xMas Projektwoche 

Es ist, als Informatiker sehr wichtig verschiedene Programme richtig anwenden zu können. Die Abläufe zu verstehen und es in der richtige schrittweise zu anwenden. Dazu muss man immer auf dem neusten Stand sein. In dieser Projektwoche untersuchen wir die beiden Websiten GitHub und GitLab, zeigen die Sprache Markdown und die Methode von DevOps. GitLab ist eine komplette DevOps-Plattform, die als eine einzige Anwendung bereitgestellt wird. GitLab hat die Arbeitsweise in den Bereichen Softwareentwicklung, Sicherheit, Betrieb und Produktentwicklung verändert, Software schneller entwickeln, testen und bereitstellen. Die GitLab-Plattform für Projektplanung, Codeverwaltung und Sicherheit bietet eine einzige zuverlässige Datenquelle für alle Benutzer, unabhängig von ihrer Rolle. Durch GitLab kann ein Unternehmen schneller vorankommen, weil die DevOps zusammenarbeiten. Applikationsentwickler und Platzformentwickler zusammen, sind sie viel schneller und produktiver.

## Inhaltsverzeichnis


1. Was ist GitHub? 
 	*	Warum GitHub
	*	Vorteile und Nachteile
	*	Wozu braucht man GitHub? 

1.  Was ist GitLab? 
	*	Warum GitLab
	*	Vorteile und Nachteile	
	*	Wozu braucht man GitLab?

1. Was ist DevOps?
	* 	Warum DevOps
	*	Vorteile und Nachteile
	* 	Wozu braucht man DevOps?

1. Was ist Markdown?
	*	Warum Markdown
	*	Vorteiel und Nachteile
	*	Wozu braucht man Markdown?	




## Was ist GitHub

GitHub ist die grösste Git basierte Steuerungsplattfrom. Es git mittlerweile über 38 Millionen Projekte und am häufigsten von Open Source Communites verwendet. Gegenüber anderen Management-Plattformen hat GitHub den Vorteil, dass es, anstatt die Anhäufung des gesamten Quellcodes in den Vordergrund zu stellen, die Aufspaltung und Zusammenführung auf besondere. Vereinfachung auch die Hilfe für externe Projekte, die Daten jedes Projektes werden in eine sogenannte Quelltextdatenbank (von Github verwendet, inklusive Repositories) zusammengeführt. Daher können Sie auch einen einzelnen Split löschen und bearbeiten, wenn der Split zu einem fremden Projekt gehört, können Sie ihn trotzdem bearbeiten und an den ursprünglichen Besitzer zurücksenden, wenn sich das Programm ändert und er eine Anfrage erhält, ob die Änderungen vorgenommen werden können Bezüglich seines ursprünglichen Programms, ob er es annehmen will oder nicht. GitHub ist eine Sammlung von Daten, die das Projekt in kleine Teile zerlegen und mit Hilfe von Fremden Verbesserungen vornehmen können. 

* GitHub sorgt für Ordnung und ist eine Versionsverwaltung.
* Nutzer können die Programme einfach herunterladen.

> **Vorteile**: 	GitHub erleitert die Fehlersuche und erstellt automatisch Fehlerberichte. Hohe Verfügbarkeit und Infrastrukturleistung. Leicht zu bedienen und Benutzerfreundlich. Zahlreiche integriestre Anwendungen.

> **Nachteil**: 	Ein Monatliches Abonnement erforderlich und in einigen Fällen auch zu Einschränkung in der Nutzbarkeit. Server hosten ist kostenpflichtig. CI/CD sind nur durch Einbeziehung Dritter möglich. 


GitHub realisiert Usern in seinen öffentlichen Repositories Software zu verändern, anzupassen und weiterzuentwickeln. Entwickler können auf GitHub als Team oder selbstständig funktionieren. Öffentliche Nutzer erstellen dafür Profile, welche ihre Respositories und öffentlichen Aktivitäten zeigen. Zudem helfen die Profile Entwicklern beim Finden von Projekten.


## Was ist GitLab

GitLab ist ähnlich wie GitHub eine Website mit grafischer Benutzeroberfläche, die ebenfalls auf Githosting basiert. GitLab ist eine Alternative zu GitHub. GitHub bietet ein Issue-Tracking-System basierend. Einer der Vorteile von GitLab ist, dass Sie nicht alle Projekte frei zugänglich machen müssen. Dafür können beispielsweise Firmen, die ihre Projekte geheim halten wollen, dies dort tun. GitLab ist eine Online-Plattform, auf der Sie alleine oder mit anderen programmieren können. Sie können beispielsweise eine Gruppe erstellen und mit anderen etwas programmieren. Sie können testen, was getan wird, oder Sie können den Code anderer Personen als Vorlagen verwenden oder anzeigen. Es gibt jedoch nicht viel Code, da es nicht so viele Benutzer hat.

> **Vorteile**: Firmen haben vorteile und können es Gratis profitieren. Gut Mobil nutzbar. Inhaltet viele Statistiken zu Projekten. CI/CD - Tools sind inbegriffen. Host kann selber erstellt werden, geregelte Rechtverwaltung. 

> **Nachteil**: Server vom GitLab sind weniger stabil als GitHub, noch nicht so weit verbreietet und die Integration der Anwendung kann problematisch sein.

GitLab ist ein webbasiertes Git-Repository, das kostenlose offene und private Repositorys, Problemverfolgungsfunktionen und Wikis bereitstellt. Es ist eine komplette DevOps Plattform, die es Fachleuten ermöglicht, alle Aufgaben in einem Projekt auszuführen von der Projektplanung und Quellcodekontrolle bis hin zu Überwachung und Sicherheit.



## Was ist DevOps

DevOps integriert Entwicklern und Betriebsteams, um die Zusammenarbeit und Prduktivität durch die Automatisierung der Infrastruktur, die Automatisierung von Arbeitsabläufen und die Messung der Anwendungsleistiung zu verbessern. DevOps ist eine Methode mit der Applikationsentwicklern und Plattformentwicklern einfach und effizient miteinader arbeiten können. 

* DevOps = New Mindset + New Tools + New Skills

> **Vorteile**: Schneller auf dem Markt. Mehr Fokus auf die Verbesserung des Geschäfts. Höherer Sicherheit durch ständiges Prüfung. Schnellerer Bereitstellung von Updates und Entwicklungsversionen.

> **Nachteil**:	Alte Strukturen werden gebrochen und müssen den DevOps methoden verwenden.


DevOps ist schneller innovativ, bessere Qulaität und schnellere Veröffentlichung. Ermöglicht es zuvor getrennten Rollen wie Entwicklung, IT-Betrieb, Qualitätstechnik und Sicherheit, sich zu koordinieren und zusammenarbeiten, um bessere und zuverlässigere Produkte zu liefern


## Was ist Markdown

Markdown ist ein Text zu HTML Konvertierungstool für Webautoren mit einer einfachen Formatsyntax. Ist ein einfacher Texteditor ähnlich wie Word, aber Sie können Sonderzeichen verwenden, um bestimmte Textzustände zu erreichen, anstatt zu klicken. Markdown macht es einfach, Dinge einfach und schnell zu machen. Je mehr Details jedoch, desto schwieriger ist die Verwendung von Markdown. Sie können Bilder auch hin fügen aber durch eine Quellencode.

> **Vorteile**: Extrem einfaches lernen. Fehlerquelle ist duch die simple Syntax sehr gering.

> **Nachteil**: Für Anwender, die gerne alles Grafisch mit der maus formatieren und die erbeniss sehen wollen, ist Markdown nicht so Opitmal.


Markdown wird überall verwendet. Wie z.p Web Hosting Servies und Communication Applications. Die meisten Webseiten sind in Markdown geschriben auch Major Webseiten. 
