![IBZ](https://getlogo.net/wp-content/uploads/2020/05/ibz-schulen-ag-logo-vector.png)
# XMas Projektwoche Teilaufgabe 1 
---

Die Informatik entwickelt sich stets weiter. Ständig werden neue Programmiersprachen entwickelt, neue Methoden werde erfunden oder neue Programme/Webseiten werden programmiert. Deswegen ist es wichtig sich als Informatiker ständig weiter zu bilden. In diesem Markdown-Dokument erläutere ich zwei Programme/Webseiten namens GitHub und GitLab, welche beide auf Git basieren was ich in diesem Text ebenfalls erklären werde. Wir schauen uns die Methode Development Operations, kurz DevOps, an und am Schluss werde ich noch erkläre, was geanu ein Markdown ist.

## Inhalt
1. **GitHub/GitLab**
    * [Was ist überhaupt Git](### Was ist überhaupt Git?)
    * [Was ist nun aber GitHub?](### Was ist nun aber GitHub?)
    * [Was ist Nun aber GitLab?](### Was ist nun aber GitHub?)
2. **Development Operations**
    * [Was ist DevOps?](### Was ist DevOps?)
    * [Wie funktioniert DevOps?](### Wie funktioniert DevOps?)
3. **Markdown**
    * [Was ist Markdown?](### Was ist Markdown?)


## **GitHub/GitLab**
---
### Was ist überhaupt Git?
Damit wir verstehen, was GitHub oder GitLab ist, nehmen wir die Wörter auseinander und, uns schauen uns zuerst an was genau Git ist. Kurz gesagt ist Git eine Software für die Versionenverwaltung. Das Prinzip von Git ist sehr gut dafür, geeignet Programme mit mehreren Personen zu bearbeiten. Um ein Projekt mit Git zu verwalten, muss man zuerst ein Repository anlegen.
Ein Repository ist in der Git-Sprache ganz einfach ein Projekt, was mit Hilfe von Git gemanagt wird. In diesem Repository befinden sich alle Dateien des Projektes. Wenn man sich jetzt an diesem Projekt beteiligen will, kann man einfach dieses Repository herunterladen und zum Beispiel Bugs beseitigen, die noch im Code waren oder weitere Funktionen ein bauen usw. Ist man nun fertig mit seinen Änderungen, kann ganz einfach ein Pull-Request machen. Ein Pull-Request bedeutet ganz einfach das der Besitzer des Repositorys sich die Änderungen anschauen kann, und somit entscheidet ob die Änderungen in das Finale Projekt implementiert werden oder nicht. Die Besonderheit an Git ist das bei jeder Veränderung eine neue Version erstellt wird und man somit also immer zu den alten Standpunkten zurückkehren kann. Man muss sich also keine Sorgen machen, dass man mit einer Veränderung das Programm kaputt macht.

### Was ist nun aber GitHub?
GitHub ist eine Webseite mit grafischer Oberfläche, die ein Githosting bereitstellt. Man kann dort also Git-Projekte hosten, diese im Browser verwalten und noch vieles mehr. Auf GitHub gibt es viele Comuity-Features. Es gibt zum Beispiel viele Open-Source Projekte, an denen man sich beteiligen kann, aber man kann auch Hilfe suchen bei seinen eigenen Projekten. Zudem geeignet sich GitHub auch, beim alleinigen Arbeiten, als Cloud der eigenen Projekte da dort, wie schon erwähnt, die Projekte in verschiedenen Versionen gespeichert werden. Ein Nachteil von GitHub ist das alle Projekte frei zugänglich sind das heisst, dass z.B. Firmen, die ihre Projekte nicht veröffentlichen wollen, dies nicht können.

### Was ist nun aber GitLab?
GitLab ist ähnlich wie GitHub eine Webseite mit grafischer Oberfläche, die ebenfalls auf dem Githosting basiert. GitLab ist die alternative zu GitHub Mit etwa dem gleichen Potenzial. GitHub bietet ein Issue-Tracking-System basierend auf Kanban-Board, ein eingebautes Wiki und noch viels mehr. Ein Vorteil von GitLab ist das man nicht alle Projekte frei zugänglich machen muss, man kann also asuwählen, ob man sein Projekt auf öffentlich oder privat stellen will Aus diesem Grund benutzten z.b. Firmen die ihre Projekte „Geheim“ halten wollen es dort tun können. Was auch noch ein Unterschied zwischen GitLab und GitHub wär, ist das GitLab Open Source ist während GitHub es nicht ist. 

## **Development Operations**
---
### Was ist DevOps?
Development Operations kurz DevOps ist eine Methode mit der Plattformentwickler und Applikationsentwickler einfach und mit viel effizient zusammen arbeiten können. Die DevOps-Teams, der oben genanten Informatik Fachrichtungen, können mit DevOps besser und einfacher auf die Wünsche und Anforderungen ihrer Kunden reagieren und eingehen.

### Wie funktioniert DevOps?
* **Planen:**
Beim Planen definieren, beschreiben und konzipieren die Teams alle Funktionen und Features die sie in die Anwendung implementieren wollen. Die Teams Planen flexible und gewähren Sicherheit mithilfe von Kanban-Board, indem sie den Fortschritt visualisieren, sie erstellen Backlogs usw.
* **Entwickeln:**
Beim Entwickeln geht es hauptsächlich um das Programmieren. Man schreibt den Code, man testet ihn, prüft ob alles in Ordnung ist usw. DevOps Teams Arbeiten effizient in dem sie Hoch produktive Tools Verwenden, Sie automatisieren alltägliche aufgaben mit kleine Programme. 
* **Lieferung:**
Lieferung bedeutet so die zuverlässige Bereitstellung der Anwendungen in den Produktionsumgebungen. Die DevOps-Teams automatisieren auch Gates welche, die Anwendung automatisch zwischen den verschiedenen Phasen wechselt, bis die Anwendung den Kunden endlich zur Verfügung steht.
* **Betreiben:**
Bei der Betriebsphase geht, es darum die Anwendung zu überwachen, zu Warten, Problem behandeln und ähnliche Dinge. Bei diesem Schritt geht es darum Zuverlässigkeit zu gewähren und dafür zu sorgen, dass die Anwendung Fehler frei funktioniert so das die Kunden die Anwendung entspannt verwenden können. 

## **Markdown**
---
### Was ist Markdown?
Markdown ist eine Art Programmiersprache die sehr simpel ist und mit der man nur Dokumente erstellen kann, welche von Aaron Swartz und John Gruber entwickelt wurde. Man kann sehr einfach und schnell simple Dokumente schreiben. Man kann Markdown sehr einfach lernen, man kann sich zum Beispiel ein oder zwei Tutorials auf YouTube anschauen oder man schaut im Internet nach wie die einzelnen Befehle lauten. Markdown erschien das erstmal im Dezember 2004. Die aktuelle Version ist die 1.0.1 (stand 16.12.2021). Es gibt mehrere Wege, wie man ein Markdown erstellt. Man kann zum Beispiel im Internet auf Webseiten wie Dillinger ganz einfach seinen Text bearbeiten. Man kann sich aber auch extra Programme dafür herunterladen oder auf z.B. Visual Code Markdown herunterladen und dort ein Markdown erstellen. Ein Nachteil von Markdown ist das man beim Bearbeiten eines Markdown kein rechtschreib Programm hat, welches einem den Text korrigiert. Das kann man aber ganz einfach umgehen in dem man seinen Text zuerst in einer Anwendung Schreibt, in der es eine Rechtschreib korrektur gibt. Wenn man den Text dann also fertig hat, kopiert man ihn einfach in den Markdown-Editor und kann dann mit den verschiedenen Markdown befehlen das Dokument schon verzieren. Markdown ist noch nicht so bekannt, aber es ist dennoch hervorragend zum Dokumenteerstellen. Es wird öfters auf GitHub oder GitLab benutzt. Man könnte Markdown mit HTML vergleichen, es ist jedoch um einiges leichter.
